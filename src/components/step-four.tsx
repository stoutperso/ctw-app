import React from "react";
import "./step-four.css";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

function StepFour({ mealCategory, nbPeople, selectedRestaurant, selectedDishes }) {
  return (
    <>
      <Box className="box">
        <Card className="card">
          <CardContent>
            <Typography className="title" color="text.secondary" gutterBottom>
              Meal
            </Typography>
            <Typography variant="h5" component="div">
              {mealCategory.charAt(0).toUpperCase() + mealCategory.slice(1)}
            </Typography>
          </CardContent>
        </Card>
        <Card className="card">
          <CardContent>
            <Typography className="title" color="text.secondary" gutterBottom>
              No. of People
            </Typography>
            <Typography variant="h5" component="div">
              {nbPeople}
            </Typography>
          </CardContent>
        </Card>
      </Box>
      <Box className="box">
        <Card className="card">
          <CardContent>
            <Typography className="title" color="text.secondary" gutterBottom>
              Restaurant
            </Typography>
            <Typography variant="h5" component="div">
              {selectedRestaurant}
            </Typography>
          </CardContent>
        </Card>
        <Card className="card">
          <CardContent>
            <Typography className="title" color="text.secondary" gutterBottom>
              Dishes
            </Typography>
            {selectedDishes.map(({ dishe, quantity }) => (
              <Typography key={dishe} variant="h5" component="div">
                {dishe} - {quantity}
              </Typography>
            ))}
          </CardContent>
        </Card>
      </Box>
    </>
  );
}

export default StepFour;
