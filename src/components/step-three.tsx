import React from "react";
import "./step-three.css";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import AddCircleIcon from "@mui/icons-material/AddCircle";

function StepThree({ selectedDishes, setSelectedDishes, availableDishes }) {
  const limitMax = (i: number) => {
    return (
      10 -
      selectedDishes.reduce((accumulator: number, { quantity }, index: number) => {
        return i === index ? accumulator : accumulator + quantity;
      }, 0)
    );
  };

  return (
    <>
      {selectedDishes.map(({ dishe, quantity }, i: number) => (
        <Box key={i} className="box">
          <FormControl className="input">
            <InputLabel id={`dish-${i}`}>Please Select a Dish</InputLabel>
            <Select
              labelId={`dish-${i}`}
              value={dishe}
              label="Please Select a Dish"
              onChange={(event) => {
                const tempDishes = [...selectedDishes];
                tempDishes.splice(i, 1, { dishe: event.target.value, quantity });
                setSelectedDishes(tempDishes);
              }}
            >
              {availableDishes
                .filter(
                  (availableDishe: string) =>
                    !selectedDishes.find((selectedDishe: any) => selectedDishe.dishe === availableDishe) ||
                    availableDishe === selectedDishes[i].dishe
                )
                .map((availableDishe: string) => (
                  <MenuItem key={availableDishe} value={availableDishe}>
                    {availableDishe}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
          <TextField
            className="input"
            value={quantity}
            label="Please enter a no. of servings"
            type="number"
            InputProps={{ inputProps: { min: 1, max: limitMax(i) } }}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(event) => {
              const tempDishes = [...selectedDishes];
              tempDishes.splice(i, 1, { dishe, quantity: +event.target.value });
              setSelectedDishes(tempDishes);
            }}
          />
        </Box>
      ))}
      <IconButton
        aria-label="add"
        size="large"
        onClick={() => {
          const tempDishes = [...selectedDishes];
          tempDishes.push({ dishe: "", quantity: 1 });
          setSelectedDishes(tempDishes);
        }}
        disabled={
          selectedDishes.reduce((accumulator: number, { quantity }) => accumulator + quantity, 0) === 10 ||
          selectedDishes.length === availableDishes.length
        }
      >
        <AddCircleIcon fontSize="inherit" />
      </IconButton>
    </>
  );
}

export default StepThree;
