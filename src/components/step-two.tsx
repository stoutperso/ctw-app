import React from "react";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";

function StepTwo({ selectedRestaurant, setSelectedRestaurant, availableRestaurants }) {
  return (
    <FormControl fullWidth>
      <InputLabel id="restaurant">Restaurant</InputLabel>
      <Select
        labelId="restaurant"
        value={selectedRestaurant}
        label="Restaurant"
        onChange={(event) => {
          setSelectedRestaurant(event.target.value as string);
        }}
      >
        {availableRestaurants.map((restau) => (
          <MenuItem key={restau} value={restau}>
            {restau}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default StepTwo;
