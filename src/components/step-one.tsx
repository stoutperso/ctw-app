import React from "react";
import "./step-one.css";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";

function StepOne({ mealCategory, setMealCategory, nbPeople, setNbPeople }) {
  return (
    <>
      <FormControl fullWidth>
        <InputLabel id="meal-category">Meal Category</InputLabel>
        <Select
          labelId="meal-category"
          value={mealCategory}
          label="Meal Category"
          onChange={(event) => {
            setMealCategory(event.target.value as string);
          }}
        >
          <MenuItem value="breakfast">Breakfast</MenuItem>
          <MenuItem value="lunch">Lunch</MenuItem>
          <MenuItem value="dinner">Dinner</MenuItem>
        </Select>
      </FormControl>
      <TextField
        className="textField"
        value={nbPeople}
        label="Number"
        type="number"
        InputProps={{ inputProps: { min: 1, max: 10 } }}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(event) => {
          setNbPeople(+event.target.value);
        }}
      />
    </>
  );
}

export default StepOne;
