import { render, screen, fireEvent } from "@testing-library/react";
import React from "react";
import App from "./App.tsx";

test("shall render properly", () => {
  render(<App />);
  expect(screen.getByText("Step 1")).toBeInTheDocument();
  expect(screen.getByText("Step 2")).toBeInTheDocument();
  expect(screen.getByText("Step 3")).toBeInTheDocument();
  expect(screen.getByText("Review")).toBeInTheDocument();
  expect(screen.getByLabelText("Meal Category")).toBeInTheDocument();
  expect(screen.getByText("Back")).toBeDisabled();
  expect(screen.getByText("Next")).toBeDisabled();
});

test("shall render step 1 properly with selected datas", () => {
  render(<App />);
  fireEvent.mouseDown(screen.getByLabelText("Meal Category"));
  fireEvent.click(screen.getByText("Lunch"));
  expect(screen.getByText("Next")).toBeEnabled();
});

test("shall render step 2 properly", () => {
  render(<App />);
  fireEvent.mouseDown(screen.getByLabelText("Meal Category"));
  fireEvent.click(screen.getByText("Lunch"));
  fireEvent.click(screen.getByText("Next"));
  expect(screen.getByText("Step 1")).toBeInTheDocument();
  expect(screen.getByText("Step 2")).toBeInTheDocument();
  expect(screen.getByText("Step 3")).toBeInTheDocument();
  expect(screen.getByText("Review")).toBeInTheDocument();
  expect(screen.getByLabelText("Restaurant")).toBeInTheDocument();
  expect(screen.getByText("Back")).toBeEnabled();
  expect(screen.getByText("Next")).toBeDisabled();
});

test("shall render step 2 properly with selected datas", () => {
  render(<App />);
  fireEvent.mouseDown(screen.getByLabelText("Meal Category"));
  fireEvent.click(screen.getByText("Lunch"));
  fireEvent.click(screen.getByText("Next"));
  fireEvent.mouseDown(screen.getByLabelText("Restaurant"));
  fireEvent.click(screen.getByText("Taco Bell"));
  expect(screen.getByText("Next")).toBeEnabled();
});
