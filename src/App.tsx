import React, { useEffect } from "react";
import "./App.css";
import StepOne from "./components/step-one.tsx";
import StepTwo from "./components/step-two.tsx";
import StepThree from "./components/step-three.tsx";
import StepFour from "./components/step-four.tsx";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";

function App() {
  const steps = ["Step 1", "Step 2", "Step 3", "Review"];
  const [activeStep, setActiveStep] = React.useState(0);
  const [mealCategory, setMealCategory] = React.useState("");
  const [nbPeople, setNbPeople] = React.useState(1);
  const [availableRestaurants, setAvailableRestaurants] = React.useState<string[]>([]);
  const [selectedRestaurant, setSelectedRestaurant] = React.useState("");
  const [availableDishes, setAvailableDishes] = React.useState<string[]>([]);
  const [selectedDishes, setSelectedDishes] = React.useState([{ dishe: "", quantity: 1 }]);

  const handleNext = () => {
    if (activeStep === steps.length - 1)
      console.log({ mealCategory, nbPeople, restaurant: selectedRestaurant, dishes: selectedDishes });
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const getAvailableRestaurants = (mealCat: string) =>
    dishes.reduce((accumulator: string[], { restaurant, availableMeals }) => {
      if (
        availableMeals.find((availableMeal) => availableMeal === mealCat) &&
        !accumulator.find((acc) => acc === restaurant)
      ) {
        return [...accumulator, restaurant];
      } else {
        return accumulator;
      }
    }, []);

  const getAvailableDishes = (restau: string, mealCat: string) =>
    dishes.reduce((accumulator: string[], { name, restaurant, availableMeals }) => {
      if (
        restaurant === restau &&
        availableMeals.find((availableMeal) => availableMeal === mealCat) &&
        !accumulator.find((acc) => acc === name)
      ) {
        return [...accumulator, name];
      } else {
        return accumulator;
      }
    }, []);

  useEffect(() => {
    setSelectedDishes([{ dishe: "", quantity: 1 }]);
    setSelectedRestaurant("");
    setAvailableRestaurants(getAvailableRestaurants(mealCategory));
  }, [mealCategory]);

  useEffect(() => {
    setSelectedDishes([{ dishe: "", quantity: 1 }]);
    setAvailableDishes(getAvailableDishes(selectedRestaurant, mealCategory));
  }, [selectedRestaurant]);

  return (
    <Box className="app">
      <Box className="stepper">
        <Stepper activeStep={activeStep}>
          {steps.map((label) => {
            const stepProps: { completed?: boolean } = {};
            return (
              <Step key={label} {...stepProps}>
                <StepLabel>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === steps.length ? (
          <React.Fragment>
            <Box className="step">Results printed in console.</Box>
            <Box className="buttons">
              <Button onClick={handleReset}>Reset</Button>
            </Box>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Box className="step">
              {activeStep === 0 && (
                <StepOne
                  mealCategory={mealCategory}
                  setMealCategory={setMealCategory}
                  nbPeople={nbPeople}
                  setNbPeople={setNbPeople}
                />
              )}
              {activeStep === 1 && (
                <StepTwo
                  selectedRestaurant={selectedRestaurant}
                  setSelectedRestaurant={setSelectedRestaurant}
                  availableRestaurants={availableRestaurants}
                />
              )}
              {activeStep === 2 && (
                <StepThree
                  selectedDishes={selectedDishes}
                  setSelectedDishes={setSelectedDishes}
                  availableDishes={availableDishes}
                  nbPeople={nbPeople}
                />
              )}
              {activeStep === 3 && (
                <StepFour
                  mealCategory={mealCategory}
                  nbPeople={nbPeople}
                  selectedRestaurant={selectedRestaurant}
                  selectedDishes={selectedDishes}
                />
              )}
            </Box>
            <Box className="buttons">
              <Button
                onClick={handleNext}
                disabled={
                  (activeStep === 0 && (!mealCategory || !nbPeople)) ||
                  (activeStep === 1 && !selectedRestaurant) ||
                  (activeStep === 2 &&
                    (selectedDishes.reduce((accumulator: number, { quantity }) => accumulator + quantity, 0) <
                      nbPeople ||
                      selectedDishes
                        .reduce((accumulator: string[], { dishe }) => [...accumulator, dishe], [])
                        .findIndex((e) => e === "") !== -1))
                }
              >
                {activeStep === steps.length - 1 ? "Send" : "Next"}
              </Button>
              <Button color="inherit" disabled={activeStep === 0} onClick={handleBack}>
                Back
              </Button>
            </Box>
          </React.Fragment>
        )}
      </Box>
    </Box>
  );
}

export default App;

const dishes = [
  {
    id: 1,
    name: "Chicken Burger",
    restaurant: "Mc Donalds",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 2,
    name: "Ham Burger",
    restaurant: "Mc Donalds",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 3,
    name: "Cheese Burger",
    restaurant: "Mc Donalds",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 4,
    name: "Fries",
    restaurant: "Mc Donalds",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 5,
    name: "Egg Muffin",
    restaurant: "Mc Donalds",
    availableMeals: ["breakfast"],
  },
  {
    id: 6,
    name: "Burrito",
    restaurant: "Taco Bell",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 7,
    name: "Tacos",
    restaurant: "Taco Bell",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 8,
    name: "Quesadilla",
    restaurant: "Taco Bell",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 9,
    name: "Steak",
    restaurant: "BBQ Hut",
    availableMeals: ["dinner"],
  },
  {
    id: 10,
    name: "Yakitori",
    restaurant: "BBQ Hut",
    availableMeals: ["dinner"],
  },
  {
    id: 11,
    name: "Nankotsu",
    restaurant: "BBQ Hut",
    availableMeals: ["dinner"],
  },
  {
    id: 12,
    name: "Piman",
    restaurant: "BBQ Hut",
    availableMeals: ["dinner"],
  },
  {
    id: 13,
    name: "Vegan Bento",
    restaurant: "Vege Deli",
    availableMeals: ["lunch"],
  },
  {
    id: 14,
    name: "Coleslaw Sandwich",
    restaurant: "Vege Deli",
    availableMeals: ["breakfast"],
  },
  {
    id: 15,
    name: "Grilled Sandwich",
    restaurant: "Vege Deli",
    availableMeals: ["breakfast"],
  },
  {
    id: 16,
    name: "Veg. Salad",
    restaurant: "Vege Deli",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 17,
    name: "Fruit Salad",
    restaurant: "Vege Deli",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 18,
    name: "Corn Soup",
    restaurant: "Vege Deli",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 19,
    name: "Tomato Soup",
    restaurant: "Vege Deli",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 20,
    name: "Minestrone Soup",
    restaurant: "Vege Deli",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 21,
    name: "Pepperoni Pizza",
    restaurant: "Pizzeria",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 22,
    name: "Pepperoni Pizza",
    restaurant: "Pizzeria",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 23,
    name: "Hawaiian Pizza",
    restaurant: "Pizzeria",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 24,
    name: "Seafood Pizza",
    restaurant: "Pizzeria",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 25,
    name: "Deep Dish Pizza",
    restaurant: "Pizzeria",
    availableMeals: ["dinner"],
  },
  {
    id: 26,
    name: "Chow Mein",
    restaurant: "Panda Express",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 27,
    name: "Mapo Tofu",
    restaurant: "Panda Express",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 28,
    name: "Kung Pao",
    restaurant: "Panda Express",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 29,
    name: "Wontons",
    restaurant: "Panda Express",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 30,
    name: "Garlic Bread",
    restaurant: "Olive Garden",
    availableMeals: ["breakfast", "lunch", "dinner"],
  },
  {
    id: 31,
    name: "Ravioli",
    restaurant: "Olive Garden",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 32,
    name: "Rigatoni Spaghetti",
    restaurant: "Olive Garden",
    availableMeals: ["lunch", "dinner"],
  },
  {
    id: 33,
    name: "Fettucine Pasta",
    restaurant: "Olive Garden",
    availableMeals: ["lunch", "dinner"],
  },
];
